package de.kidandroid.Entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DepartmentEntity (
    @PrimaryKey var departmentId: Int,
    var departmentName: String,
    var order: Int,
    var subDepartments: List<Int>?,
    var parentDepartment: Int?
) {}