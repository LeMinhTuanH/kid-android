package de.kidandroid.Entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(
    entity = RoomEntity::class,
    parentColumns = ["roomId"],
    childColumns = ["room"],
    onDelete = ForeignKey.NO_ACTION
)]
)
data class EmployeeEntity (
    @PrimaryKey var employeeId: Int,
    var employeeName: String,
    var employeePhone: String,
    var employeeMail: String,

    var room: Int
){}