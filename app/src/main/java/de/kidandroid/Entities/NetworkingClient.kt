package de.kidandroid.Entities

import com.google.gson.GsonBuilder
import de.kidandroid.Model.DepartmentTask
import de.kidandroid.Room.KIDRepository
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

class NetworkingClient() {

    private fun getInitData() {
        val url = "http://ludwig.danubius0282.nbsp.de/api/init/682TM5O15W3H757o75To4d7c68121532"
//        val url = "http://ludwig.danubius0282.nbsp.de/api/init/G8Kpn30e813173s1cg823787F3SA7Ddi"

        val request = Request.Builder().url(url).build()

        val client =
            OkHttpClient().newBuilder().connectTimeout(5, TimeUnit.SECONDS).retryOnConnectionFailure(false).build()

        client.newCall(request).enqueue(object : okhttp3.Callback {

            override fun onFailure(call: Call, e: IOException) {
                println("onFailure")
                println(e)
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                body?.let {
                    parseJson(it)
                }
            }
        })
    }

        private fun parseJson(json: String) {
        if (json != null) {
            val gson = GsonBuilder().create()

            val departmentsResult = gson.fromJson(json, JsonDepartmentsResponse::class.java)
            val employeeResult = gson.fromJson(json, JsonEmployeesResponse::class.java)
            val roomResult = gson.fromJson(json, JsonRoomResponse::class.java)
            val routerResult = gson.fromJson(json, JsonRouterResponse::class.java)
            val departmentTaskResult = gson.fromJson(json, JsonDepartmentTaskResponse::class.java)

            ioThread {
                var repo = KIDRepository()
                for (department in departmentsResult.departments) {
//
                    department.subDepartments?.let {
                        for (subDepartment in it) {
                            val filteredList = departmentsResult.departments.filter { it.departmentId == subDepartment }
                            filteredList.first().parentDepartment = department.departmentId
                        }
                    }

                    repo.saveDepartment(department)
                }

                repo.deleteAllRouter()
                for (router in routerResult.routers) {
                    val routerEntity = RouterEntity(router.routeId,router.routeName,router.routeDescription)
                    repo.saveRouter(routerEntity)
//                    router.routeFiles?.let {
                        //                        for (routerFile in it) {
//                            val routerFileEntity = RouterFileEntity(routerFile.routeGraphicFile,routerFile.routeGraphicDescription,router.routeId)
//                            repo.saveRouterFile(routerFileEntity)
//                        }
//                    }
                }

                for (room in roomResult.rooms) {
                    repo.saveRoom(room)
                }

                for (employee in employeeResult.employees) {
                    repo.saveEmployee(employee)
                }
            }
        }
    }
}

class JsonDepartmentsResponse(val departments: List<DepartmentEntity>)
class JsonEmployeesResponse(val employees: List<EmployeeEntity>)
class JsonRoomResponse(val rooms: List<RoomEntity>)
class JsonRouterResponse(val routers: List<RouterEntity>)
class JsonDepartmentTaskResponse(val tasks: List<DepartmentTask>)