package de.kidandroid.Entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(
    entity = RouterEntity::class,
    parentColumns = ["routeId"],
    childColumns = ["router"],
    onDelete = ForeignKey.SET_NULL
)]
)

data class RoomEntity (
    @PrimaryKey var roomId: Int,
    var roomNumber: String,
    var router: Int?
){}