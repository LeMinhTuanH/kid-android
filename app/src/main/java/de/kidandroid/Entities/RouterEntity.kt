package de.kidandroid.Entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RouterEntity (
    @PrimaryKey var routeId: Int,
    var routeName: String,
    var routeDescription: String
){}
