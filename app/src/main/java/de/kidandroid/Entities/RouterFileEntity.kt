package de.kidandroid.Entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(
    entity = RouterEntity::class,
    parentColumns = ["routeId"],
    childColumns = ["routerId"],
    onDelete = ForeignKey.CASCADE
)]
)

data class RouterFileEntity (
    @PrimaryKey(autoGenerate = true)
    var routerFileEntityId: Int,
    var routeGraphicFile: String,
    var routeGraphicDescription: String,
    var routerId: Int
){}