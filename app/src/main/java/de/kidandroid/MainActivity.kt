package de.kidandroid

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.kidandroid.Model.*
import de.kidandroid.View.Department.DepartmentGridCardAdapter
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Wegweiser"

        getInitData()
//        getInitDataLocal()
    }

    private fun parseJson(jsonModel: String) {
        ModelStore.parseJsonModel(jsonModel)
        departmentList.layoutManager = GridLayoutManager(this, 3, RecyclerView.VERTICAL,false)
        departmentList.adapter = DepartmentGridCardAdapter(ModelStore.getTopdepartments().toMutableList())
    }

    private fun getInitDataLocal() {

        try {
            val inputStream: InputStream = assets.open("G8Kpn30e813173s1cg823787F3SA7Ddi.json")
            val jsonModel = inputStream.bufferedReader().use { it.readText() }
            parseJson(jsonModel)
        } catch (e: Exception) {
        }
    }

    private fun getInitData() {
        val url = "http://ludwig.danubius0282.nbsp.de/api/init/682TM5O15W3H757o75To4d7c68121532"
//        val url = "http://ludwig.danubius0282.nbsp.de/api/init/G8Kpn30e813173s1cg823787F3SA7Ddi"

        val request = Request.Builder().url(url).build()

        val client =
            OkHttpClient().newBuilder().connectTimeout(5, TimeUnit.SECONDS).retryOnConnectionFailure(false).build()

        client.newCall(request).enqueue(object : okhttp3.Callback {

            override fun onFailure(call: Call, e: IOException) {
                println("onFailure")
                println(e)
//                runOnUiThread {
//                    getInitDataLocal()
//                }
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                body?.let {
                    runOnUiThread {
                        parseJson(it)
                    }
                }
            }
        })
    }


//    private fun testDatabase() {
//        ioThread {
//            var repo = KIDRepository()
//            val departments = repo.getAllDepartments()
//
//            runOnUiThread {
//                for (department in departments) {
//                    println(department.departmentName)
//                }
//            }
//        }
//    }
}
