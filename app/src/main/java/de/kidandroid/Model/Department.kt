package de.kidandroid.Model

data class Department (
    var departmentId: Int,
    var departmentName: String,
    var order: Int,
    var employees: MutableList<Employee>,
    var subDepartments: List<Int>?,
    var subDepartmentsObject: MutableList<Department>,
    var parentDepartment: Department?
) {}