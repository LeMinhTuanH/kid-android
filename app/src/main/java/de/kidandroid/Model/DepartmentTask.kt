package de.kidandroid.Model

data class DepartmentTask (
    var taskId: Int,
    var taskName: String,
    var taskJobTitle: String
){}