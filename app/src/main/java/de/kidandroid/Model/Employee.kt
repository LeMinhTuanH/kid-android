package de.kidandroid.Model

data class Employee (
    var employeeId: Int,
    var employeeName: String,
    var employeePhone: String,
    var employeeMail: String,
    var room: Int,
    var roomObject: Room,
    var departments: List<DepartmentsInfo>?,
    var tasks: List<TasksInfo>?
){}

data class DepartmentsInfo (
    var departmentId: Int,
    var departmentJobTitle: String
)

data class TasksInfo (
    var taskId: Int,
    var taskJobTitle: String
)