package de.kidandroid.Model

import android.app.Application
import com.google.gson.GsonBuilder
import de.kidandroid.Room.KIDApplication
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.TimeUnit

object ModelStore {
    private lateinit var allDepartments: List<Department>
    private lateinit var allEmployee: List<Employee>
    private lateinit var allRoom: List<Room>
    private lateinit var allRouter: List<Router>
    fun parseJsonModel(jsonModel: String) {
        parseJson(jsonModel)
    }

    fun getTopdepartments(): List<Department> {
        return allDepartments.filter { it.parentDepartment == null }.sortedBy { it.departmentName }
    }

    fun getEmployeesFromDepartment(departmentId: Int): List<Employee> {
        val department = allDepartments.first { it.departmentId == departmentId }

        if (department.employees == null) {
            return listOf<Employee>()
        }
        return department.employees
    }

    fun getSubdepartments(parentDepartmentId: Int): List<Department> {
        val department = allDepartments.first { it.departmentId == parentDepartmentId }

        if (department.subDepartmentsObject == null) {
            return listOf<Department>()
        }
        return department.subDepartmentsObject
    }

    fun getEmployee(employeeId: Int): Employee {
        return allEmployee.first { it.employeeId == employeeId }
    }



    private fun parseJson(json: String) {
        if (json != null) {
            val gson = GsonBuilder().create()

            val departmentsResult = gson.fromJson(json, JsonDepartmentsResponse::class.java)
            val employeeResult = gson.fromJson(json, JsonEmployeesResponse::class.java)
            val roomResult = gson.fromJson(json, JsonRoomResponse::class.java)
            val routerResult = gson.fromJson(json, JsonRouterResponse::class.java)
//            val departmentTaskResult = gson.fromJson(json, JsonDepartmentTaskResponse::class.java)

            val allDepartments = departmentsResult.departments
            val allEmployees = employeeResult.employees
            val allRoom = roomResult.rooms
            val allRouter = routerResult.routers

            var allDepartmentsInfos = mutableListOf<DepartmentsInfo>()
            for (department in allDepartments) {
                department.subDepartments?.let {
                    for (subDepartment in it) {
                        val filteredList = allDepartments.filter { it.departmentId == subDepartment }
                        filteredList.first().parentDepartment = department
                        if (department.subDepartmentsObject != null) {
                            department.subDepartmentsObject!!.add(filteredList.first())
                        }
                        else {
                            department.subDepartmentsObject = mutableListOf(filteredList.first())
                        }
                    }
                }
            }

            for (employee in allEmployees) {
                employee.departments?.let {
                    allDepartmentsInfos.addAll(it)

                    for (departmentInfo in it) {
                        val departmentId = departmentInfo.departmentId
                        val filteredList = allDepartments.filter { it.departmentId == departmentId }
                        if (filteredList.first().employees != null) {
                            filteredList.first().employees.add(employee)
                        }
                        else {
                            filteredList.first().employees = mutableListOf(employee)
                        }
                    }
                }
                val room = allRoom.first { it.roomId == employee.room }
                employee.roomObject = room
            }

            for (room in allRoom) {
                room.router?.let { routerId ->
                    val router = allRouter.first { it.routeId == routerId }
                    room.routerObject = router
                }
            }

            for (router in allRouter) {
                println(router.routeDescription)
            }

            this.allDepartments = allDepartments
            this.allEmployee = allEmployees
            this.allRoom = allRoom
            this.allRouter = allRouter
        }
    }
}

class JsonDepartmentsResponse(val departments: List<Department>)
class JsonEmployeesResponse(val employees: List<Employee>)
class JsonRoomResponse(val rooms: List<Room>)
class JsonRouterResponse(val routers: List<Router>)
class JsonDepartmentTaskResponse(val tasks: List<DepartmentTask>)