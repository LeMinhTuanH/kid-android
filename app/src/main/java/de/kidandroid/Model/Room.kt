package de.kidandroid.Model

data class Room (
    var roomId: Int,
    var roomNumber: String,
    var router: Int?,
    var routerObject: Router?,
    var employees: List<Employee>
){}