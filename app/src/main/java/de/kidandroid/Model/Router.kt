package de.kidandroid.Model

data class Router (
    var routeId: Int,
    var routeName: String,
    var routeDescription: String,

    var routeFiles: List<RouterFile>
){}
