package de.kidandroid.Model

data class RouterFile (
    val routeGraphicFile: String,
    val routeGraphicDescription: String
){}