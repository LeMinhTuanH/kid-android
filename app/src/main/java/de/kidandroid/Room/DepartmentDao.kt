package de.kidandroid.Room

import androidx.room.*
import de.kidandroid.Entities.DepartmentEntity

@Dao
interface DepartmentDao {
    @Query("SELECT * FROM DepartmentEntity")
    fun getAllDepartment(): List<DepartmentEntity>

    @Query("SELECT * FROM DepartmentEntity where departmentId = :departmentId")
    fun getDepartment(departmentId: Int): DepartmentEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDepartment(department: DepartmentEntity)

    @Update
    fun update(department: DepartmentEntity)

    @Delete
    fun deleteDepartment(department: DepartmentEntity)
}