package de.kidandroid.Room

import androidx.room.*
import de.kidandroid.Entities.EmployeeEntity

@Dao
interface EmployeeDao: BaseDao<EmployeeEntity> {
    @Query("SELECT * FROM EmployeeEntity")
    fun getAllEmployee(): List<EmployeeEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployee(employee: EmployeeEntity)

//    @Delete
//    fun deleteEmployee(employee: Employee)

    @Query("DELETE FROM EmployeeEntity")
    fun deleteAllEmployee()
}