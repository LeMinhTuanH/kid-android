package de.kidandroid.Room

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.squareup.picasso.Picasso

class KIDApplication: Application() {
    private var picassoSetup = false

    companion object {
        lateinit var database: KIDDatabase

    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this, KIDDatabase::class.java, "kid-db").build()

        setupPicasso()
    }

    private fun setupPicasso() {
        if (!picassoSetup) {
            val picasso = Picasso.Builder(applicationContext)
                .build()
//            picasso.setIndicatorsEnabled(true)
            Picasso.setSingletonInstance(picasso)
            picassoSetup = true
        }
    }
}