package de.kidandroid.Room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.kidandroid.Entities.*


@Database(entities = [DepartmentEntity::class, EmployeeEntity::class, RoomEntity::class, RouterEntity::class, RouterFileEntity::class],version = 1)
@TypeConverters(SubDepartmentsConverter::class)

abstract class KIDDatabase: RoomDatabase() {
    abstract fun departmentDao(): DepartmentDao
    abstract fun employeeDao(): EmployeeDao
    abstract fun routerDao(): RouterDao
    abstract fun routerFileDao(): RouterFileDao
    abstract fun roomDao(): RoomDao
}