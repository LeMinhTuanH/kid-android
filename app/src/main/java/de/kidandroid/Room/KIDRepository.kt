package de.kidandroid.Room

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import de.kidandroid.Entities.*

class KIDRepository {

    private val departmentDao: DepartmentDao = KIDApplication.database.departmentDao()
    private val employeeDao: EmployeeDao = KIDApplication.database.employeeDao()
    private val routerDao: RouterDao = KIDApplication.database.routerDao()
    private val routerFileDao: RouterFileDao = KIDApplication.database.routerFileDao()
    private val roomDao: RoomDao = KIDApplication.database.roomDao()

    private val allDepartments: List<DepartmentEntity>

    init {
        allDepartments = departmentDao.getAllDepartment()
    }

//    fun findGolfclubById(id: String) : LiveData<GolfclubEntity> {
//        return golfclubDao.findGolfclubById(id)
//    }

    fun saveRoom(room: RoomEntity) {
        roomDao.insertRoom(room)
    }

    fun saveDepartment(department: DepartmentEntity) {
        departmentDao.insertDepartment(department)
    }

    fun updateDepartment(department: DepartmentEntity) {
        departmentDao.update(department)
    }

    fun getDepartment(departmentId: Int): DepartmentEntity {
        return departmentDao.getDepartment(departmentId)
    }

    fun getAllDepartments() = allDepartments

    fun saveEmployee(employee: EmployeeEntity) {
        employeeDao.insert(employee)
    }

    fun saveRouter(router: RouterEntity) {
        routerDao.insertRouter(router)
    }

    fun deleteAllRouter() {
        routerDao.deleteAllRouter()
    }

    fun saveRouterFile(routerFile: RouterFileEntity) {
        routerFileDao.insertRouterFile(routerFile)
    }

//    private class InsertAsyncTask internal constructor(private val dao: DepartmentDao) : AsyncTask<Department, Void, Void>() {
//        override fun doInBackground(vararg params: Department): Void? {
//            dao.insertDepartment(params[0])
//            return null
//        }
//    }

//    private class DeleteAsyncTask internal constructor(private val dao: GolfclubDao) : AsyncTask<GolfclubEntity, Void, Void>() {
//        override fun doInBackground(vararg params: GolfclubEntity): Void? {
//            dao.deleteGolfclub(*params)
//            return null
//        }
//    }
}