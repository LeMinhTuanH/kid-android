package de.kidandroid.Room

import de.kidandroid.Entities.RoomEntity

import androidx.room.*

@Dao
interface RoomDao {
    @Query("SELECT * FROM RoomEntity")
    fun getAllRoom(): List<RoomEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoom(room: RoomEntity)

    @Delete
    fun deleteRoom(room: RoomEntity)

    @Query("DELETE FROM RoomEntity")
    fun deleteAllRoom()
}