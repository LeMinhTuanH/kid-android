package de.kidandroid.Room

import androidx.room.*
import de.kidandroid.Entities.RouterEntity

@Dao
interface RouterDao {
    @Query("SELECT * FROM RouterEntity")
    fun getAllRouter(): List<RouterEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRouter(router: RouterEntity)

    @Delete
    fun deleteRouter(router: RouterEntity)

    @Query("DELETE FROM RouterEntity")
    fun deleteAllRouter()
}