package de.kidandroid.Room

import androidx.room.*
import de.kidandroid.Entities.RouterFileEntity

@Dao
interface RouterFileDao {
    @Query("SELECT * FROM RouterFileEntity")
    fun getAllRouterFile(): List<RouterFileEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRouterFile(routerFile: RouterFileEntity)

    @Delete
    fun deleteRouterFile(routerFile: RouterFileEntity)
}