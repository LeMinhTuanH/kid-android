package de.kidandroid.Room

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class SubDepartmentsConverter{
    @TypeConverter
    fun stringToListInt (value: String): List<Int> {
//        val listPlayers = object : TypeToken<Int>() {}.type
        return Gson().fromJson(value, List::class.java) as List<Int>
    }

    @TypeConverter
    fun listIntToString(list: List<Int>): String {
        val gson = GsonBuilder().create()
        return gson.toJson(list)
    }
}