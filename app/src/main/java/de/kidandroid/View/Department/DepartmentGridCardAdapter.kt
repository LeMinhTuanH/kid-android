package de.kidandroid.View.Department

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.kidandroid.Model.Department
import de.kidandroid.R
import de.kidandroid.app.inflate
import kotlinx.android.synthetic.main.department_grid_card.view.*

class DepartmentGridCardAdapter(private val departments: MutableList<Department>): RecyclerView.Adapter<DepartmentGridCardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentGridCardViewHolder {
        return DepartmentGridCardViewHolder(parent.inflate(R.layout.department_grid_card))
    }
    override fun getItemCount() = departments.size

    override fun onBindViewHolder(holder: DepartmentGridCardViewHolder, position: Int) {
        holder.bind(departments[position])
    }

    fun updateDepartments(departments: List<Department>) {
        this.departments.clear()
        this.departments.addAll(departments)
        notifyDataSetChanged()
    }
}

open class DepartmentGridCardViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
    protected lateinit var department: Department

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        view?.let {
            val intent = DepartmentOverView.newIntent(view.context,department.departmentId,department.departmentName)
            view.context.startActivity(intent)
        }
    }

    fun bind(department: Department) {
        this.department = department
        itemView.departmentName.text = department.departmentName
    }
}