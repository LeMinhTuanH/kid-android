package de.kidandroid.View.Department

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import de.kidandroid.Model.ModelStore
import de.kidandroid.R
import de.kidandroid.View.Employee.EmployeeGridCardAdapter
import kotlinx.android.synthetic.main.activity_department_over_view.*

class DepartmentOverView : AppCompatActivity() {

    companion object {
        private const val DEPARTMENT_ID = "DEPARTMENT_ID"
        private const val DEPARTMENT_NAME = "DEPARTMENT_NAME"

        fun newIntent(context: Context, departmentId: Int, departmentName: String): Intent {
            val intent = Intent(context, DepartmentOverView::class.java)
            intent.putExtra(DEPARTMENT_ID, departmentId)
            intent.putExtra(DEPARTMENT_NAME, departmentName)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_department_over_view)
        title = intent.getStringExtra(DEPARTMENT_NAME)

        val departmentId = intent.getIntExtra(DEPARTMENT_ID,0)

        subDepartmentsList.layoutManager = GridLayoutManager(this,1)
        subDepartmentsList.adapter = SubdepartmentGridCardAdapter(ModelStore.getSubdepartments(departmentId).toMutableList())

        departmentEmployeesList.layoutManager = GridLayoutManager(this,2)
        departmentEmployeesList.adapter =
            EmployeeGridCardAdapter(ModelStore.getEmployeesFromDepartment(departmentId))
    }
}
