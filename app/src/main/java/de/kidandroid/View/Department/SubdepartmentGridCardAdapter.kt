package de.kidandroid.View.Department

import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import de.kidandroid.Model.Department
import de.kidandroid.R
import de.kidandroid.app.inflate

class SubdepartmentGridCardAdapter(private val departments: MutableList<Department>): RecyclerView.Adapter<SubdepartmentGridCardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubdepartmentGridCardViewHolder {
        return SubdepartmentGridCardViewHolder(parent.inflate(R.layout.department_grid_card))
    }
    override fun getItemCount() = departments.size

    override fun onBindViewHolder(holder: SubdepartmentGridCardViewHolder, position: Int) {
        holder.bind(departments[position])
    }

    fun updateSubdepartments(subdepartments: List<Department>) {
        this.departments.clear()
        this.departments.addAll(departments)
        notifyDataSetChanged()
    }
}

class SubdepartmentGridCardViewHolder(view: View) : DepartmentGridCardViewHolder(view) {
    override fun onClick(view: View?) {
        view?.let {
            val toast = Toast.makeText(view.context, department.departmentName, Toast.LENGTH_SHORT)
            toast.show()
        }
    }

}