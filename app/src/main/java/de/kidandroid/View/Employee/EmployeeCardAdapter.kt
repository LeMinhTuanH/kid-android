package de.kidandroid.View.Employee

import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import de.kidandroid.Model.Employee
import de.kidandroid.R
import de.kidandroid.app.inflate
import kotlinx.android.synthetic.main.employee_grid_card.view.*

class EmployeeGridCardAdapter(private val employees: List<Employee>): RecyclerView.Adapter<EmployeeGridCardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeGridCardViewHolder {
        return EmployeeGridCardViewHolder(parent.inflate(R.layout.employee_grid_card))
    }
    override fun getItemCount() = employees.size

    override fun onBindViewHolder(holder: EmployeeGridCardViewHolder, position: Int) {
        holder.bind(employees[position])
    }

//    fun updateDepartments(departments: List<Department>) {
//        this.departments.clear()
//        this.departments.addAll(departments)
//        notifyDataSetChanged()
//    }
}

class EmployeeGridCardViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
    private lateinit var employee: Employee

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        view?.let {
            val intent = EmployeeDetailView.newIntent(view.context,employee.employeeId, employee.employeeName)
            view.context.startActivity(intent)
        }
    }

    fun bind(employee: Employee) {
        this.employee = employee
        itemView.employeeName.text = employee.employeeName
    }
}