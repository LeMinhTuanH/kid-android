package de.kidandroid.View.Employee

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import de.kidandroid.Model.Employee
import de.kidandroid.Model.ModelStore
import de.kidandroid.R
import kotlinx.android.synthetic.main.employee_details.*
import androidx.recyclerview.widget.PagerSnapHelper

class EmployeeDetailView : AppCompatActivity() {

    private lateinit var employee: Employee
    companion object {
        private const val EMPLOYEE_ID = "EMPLOYEE_ID"
        private const val EMPLOYEE_NAME = "EMPLOYEE_NAME"

        fun newIntent(context: Context, employeeId: Int, employeeName: String): Intent {
            val intent = Intent(context, EmployeeDetailView::class.java)
            intent.putExtra(EMPLOYEE_ID, employeeId)
            intent.putExtra(EMPLOYEE_NAME, employeeName)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.employee_details)
        title = intent.getStringExtra(EMPLOYEE_NAME)
        employeeName.text = intent.getStringExtra(EMPLOYEE_NAME)

//        employee = ModelStore.getEmployee(intent.getIntExtra(EMPLOYEE_ID,0))

        employee = ModelStore.getEmployee(1169)

        employee.roomObject.routerObject?.let {
            routerList.layoutManager = GridLayoutManager(this,1)
            routerList.adapter = RouterFileAdapter(employee.roomObject.routerObject!!.routeFiles)

            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(routerList)
        }

    }
}
