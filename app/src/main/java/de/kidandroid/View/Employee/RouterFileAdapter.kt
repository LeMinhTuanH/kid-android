package de.kidandroid.View.Employee

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.kidandroid.Model.RouterFile
import de.kidandroid.R
import de.kidandroid.app.inflate
import kotlinx.android.synthetic.main.router_file_grid.view.*
import org.jsoup.Jsoup

class RouterFileAdapter(private val routerfiles: List<RouterFile>): RecyclerView.Adapter<RouterFileAdapter.RouterFileGridViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RouterFileGridViewHolder {
        return RouterFileGridViewHolder(parent.inflate(R.layout.router_file_grid))
    }
    override fun getItemCount() = routerfiles.size

    override fun onBindViewHolder(holder: RouterFileGridViewHolder, position: Int) {
        holder.bind(routerfiles[position])
    }

    class RouterFileGridViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private lateinit var routerFile: RouterFile

        fun bind(routerfile: RouterFile) {
            this.routerFile = routerfile
            itemView.routerfileDescription.text = Jsoup.parse(routerfile.routeGraphicDescription).text()

            Picasso.get().load(routerfile.routeGraphicFile).into(itemView.routerImage)
        }

    }
}

